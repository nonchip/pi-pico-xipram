#!/bin/sh
echo -n 'xip_funcs = 0x' > xip_funcs_addr.ld
arm-none-eabi-readelf -s xip.elf | grep xip_funcs | grep OBJECT | cut -d: -f 2 | cut -d' ' -f 2 >> xip_funcs_addr.ld
echo ';' >> xip_funcs_addr.ld