#include <stdio.h>
#include "pico/stdlib.h"
#include "pico/bootrom.h"

#include "xip.h"

const uint LED_PIN = PICO_DEFAULT_LED_PIN;

void blinkinit() {
  gpio_init(LED_PIN);
  gpio_set_dir(LED_PIN, GPIO_OUT);
}

void blinkoff() {
  gpio_put(LED_PIN, 0);
}

void blinkon() {
  gpio_put(LED_PIN, 1);
}

const struct xip_funcs_t xip_funcs = {
  &blinkinit,
  &blinkoff,
  &blinkon,
};


int main() {
  stdio_init_all();
  printf("XIP booted, please load RAM image!\n");
  reset_usb_boot(0, 0);
}
