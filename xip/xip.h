#pragma once

#ifndef PICO_DEFAULT_LED_PIN
#error blink example requires a board with a regular LED
#endif

struct xip_funcs_t{
  void (*init)();
  void (*off)();
  void (*on)();
};

extern const struct xip_funcs_t xip_funcs;

