#include "enable_xip.h"
#include "pico/bootrom.h"

void __no_inline_not_in_flash_func(enable_xip_via_bootrom)(){
  void (*connect_internal_flash)(void) = (void(*)(void))rom_func_lookup(rom_table_code('I', 'F'));
  void (*flash_exit_xip)(void) = (void(*)(void))rom_func_lookup(rom_table_code('E', 'X'));
  void (*flash_flush_cache)(void) = (void(*)(void))rom_func_lookup(rom_table_code('F', 'C'));
  void (*flash_enter_cmd_xip)(void) = (void(*)(void))rom_func_lookup(rom_table_code('C', 'X'));
  assert(connect_internal_flash && flash_enter_cmd_xip && flash_exit_xip && flash_flush_cache);

  __compiler_memory_barrier();
  connect_internal_flash();
  __compiler_memory_barrier();
  flash_exit_xip();
  __compiler_memory_barrier();
  flash_flush_cache();
  __compiler_memory_barrier();
  flash_enter_cmd_xip();
  __compiler_memory_barrier();
}

void __no_inline_not_in_flash_func(enable_xip_via_boot2_in_xip)(){
  void (*flash_exit_xip)(void) = (void(*)(void))rom_func_lookup(rom_table_code('E', 'X'));
  assert(flash_exit_xip);

  enable_xip_via_bootrom();

  static uint32_t boot2_copyout[64];
  for (int i = 0; i < 64; ++i){
    boot2_copyout[i] = ((uint32_t *)XIP_BASE)[i];
  }

  __compiler_memory_barrier();
  flash_exit_xip();
  __compiler_memory_barrier();
  ((void (*)(void))boot2_copyout+1)();
  __compiler_memory_barrier();
}
