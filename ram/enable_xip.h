#pragma once
#include "pico/stdlib.h"

void __no_inline_not_in_flash_func(enable_xip_via_bootrom)();
void __no_inline_not_in_flash_func(enable_xip_via_boot2_in_xip)();
