#include <stdio.h>

#include "pico/stdlib.h"
#include "hardware/watchdog.h"

#include "enable_xip.h"
#include "../xip/xip.h"

int main() {
  stdio_init_all();

  printf("setting up watchdog: ");
  watchdog_enable(2000,1);
  printf("done\n");

  enable_xip_via_boot2_in_xip();

  watchdog_update();
  sleep_ms(1000);
  printf("xip_funcs* %x\n",&xip_funcs);
  printf("xip_funcs: %x\n",xip_funcs);
  printf("     init: %x\n",xip_funcs.init);
  printf("       on: %x\n",xip_funcs.on);
  printf("      off: %x\n",xip_funcs.off);
  printf("\n");
  watchdog_update();
  sleep_ms(1000);

  xip_funcs.init();
  while (true) {
    xip_funcs.on();
    sleep_ms(250);
    xip_funcs.off();
    sleep_ms(250);
    watchdog_update();
  }
}
