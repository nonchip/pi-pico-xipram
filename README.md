# Accessing XIP while running from RAM on the Raspberry Pico

## The idea:

sometimes you want some code or data in Flash, while you run from RAM.
one example is a project I'm currently working on, which requires some common "low level" code and data, but also requires constantly changing the actual logic running, so I don't want to wear out the Flash constantly.

## The solution:

Providing 2 helper functions:
* `void enable_xip_via_bootrom()`
  * always available
  * enables "slow" XIP via the chip's builtin bootloader
* `void enable_xip_via_boot2_in_xip()`
  * available ONLY if there's actually a binary in XIP that contains the `boot2` stub (all binaries of the `default` type do this)
  * enables "fast" (flash chip specific) XIP via the `boot2` stub *loaded from flash via slow XIP*

See [`enable_xip.h`](ram/enable_xip.h) and [`enable_xip.c`](ram/enable_xip.c)

## This repository:

Implements the official `blink` demo (plus some debug serial output and watchdog facility) with the actual GPIO access to the LEDs provided via functions in XIP Flash, and the loop calling them residing in RAM.

### How it works:

* the XIP code:
  * defines functions to turn the LED on/off
  * puts their pointers in a global `const struct`
  * and automatically resets into the bootloader if booted (to make loading RAM easier)
* the build process:
  * for XIP:
    * prevents this struct from being garbage collected
    * generates a tiny linker script containing the address of this struct
  * for RAM:
    * links using that script to make the struct's address known
* then the RAM code:
  * initializes XIP access using `enable_xip_via_boot2_in_xip` (see above)
  * can then access that struct and therefore the functions simply by including the header file they were declared in.
  * loops as in the `blink` demo but instead of setting the LED's GPIO directly calls those functions

### Usage:

```sh
mkdir build
cd build
cmake ..
make
```

* Put the Pico in bootloader mode (hold down the BOOTSEL button while plugging it in).
* Load `build/xip/xip.uf2`.
  * This binary will automatically reboot back into BOOTSEL mode each time it starts, so you don't have to hold down the button anymore.
* Load `build/ram/ram.uf2`.
* It should start blinking the onboard LED at 2Hz.

